﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace whyisthissohard
{
    class Program
    {
        static void Main(string[] args)
        {

            RegexParty rp = new RegexParty();

            int pathtester = 0;
            //ask for the text file and prevent some errors:
            Console.WriteLine("'Ello, there! May I have a path to your text file, please?:");
            string textfile = Console.ReadLine();
            while (pathtester == 0)
            {
                try
                {
                    var test = File.OpenText(@textfile);
                    pathtester = 1;
                }
                catch
                {
                    Console.WriteLine("Your file path was no good! Try it again:");
                    textfile = Console.ReadLine();
                }
            }

            //ask for the regular expression and store:
            Console.WriteLine("What regular expression suits your fancy today?:");
            string soregular = Console.ReadLine();
            while (soregular.Length == 0)
            {

                Console.WriteLine("You didn't submit anything! Try again, please:");
                soregular = Console.ReadLine();
            }

            //ask for the replacement and store:
            Console.WriteLine("OK, what do you want to use as a replacement?");
            string replacement = Console.ReadLine();
            while (replacement.Length == 0)
            {
                Console.WriteLine("You have to put something here. Try again!:");
                replacement = Console.ReadLine();
            }

            rp.WhatIsIt(soregular, textfile, replacement);


            Console.WriteLine("You're done!");
            Console.ReadKey();


        }

    }

}
