﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace whyisthissohard
{
    class RegexParty
    {

        public void WhatIsIt(string soregular, string textfile, string replacement)
        {
            Char[] whatarray = soregular.ToCharArray();
            for (int j = 0; j < whatarray.Length; j++)
            {
                if (whatarray[j] == '.')
                {
                    Concat(soregular, textfile, replacement);
                    break;

                }
                if (whatarray[j] == '+')
                {
                    Union(soregular, textfile, replacement);
                }

                if (whatarray[j] == '*')
                    Kleene(soregular, textfile, replacement);

            }
        }



        //KLEENE STAR!!!!!!!

        public void Kleene(string soregular, string textfile, string replacement)
        {

            Char[] arraytime = soregular.ToCharArray();
            List<char> startime = new List<char>();


            for (int j = 0; j < arraytime.Length; j++)
            {
                if (arraytime[j] != '(' && arraytime[j] != ')' && arraytime[j] != '*')
                {
                    startime.Add(arraytime[j]);

                }
                if (arraytime[j] == '*')
                    break;

            }
            StringBuilder builder = new StringBuilder();
            foreach (char z in startime)
            {
                builder.Append(z);
            }
            string starresult = builder.ToString();


            using (var sourceFile = File.OpenText(@textfile))
            {
                StreamReader sr = new StreamReader(@textfile);
                string line = sr.ReadToEnd();

                while ((sourceFile.ReadLine()) != null)
                {
                    line = line.Replace(starresult, replacement);
                    Console.WriteLine(line);
                }
            }

        }

        //UNION!!!!!!!!!!!!
        public void Union(string soregular, string textfile, string replacement)
        {
            Char[] unionarray = soregular.ToCharArray();
            List<char> unlist = new List<char>();

            foreach (char u in unionarray)
            {
                if (u != '+' && u != '(' && u != ')' && u != '.')
                    unlist.Add(u);
                else
                    continue;

            }
            using (var sourceFile = File.OpenText(@textfile))
            {
                StreamReader sr = new StreamReader(@textfile);
                string line = sr.ReadToEnd();

                while ((sourceFile.ReadLine()) != null)
                    foreach (char o in unlist)
                    {

                        line = line.Replace(o.ToString(), replacement);

                    }
                Console.WriteLine(line);

            }

        }


        //CONCATENATION!!!!!!
        public void Concat(string soregular, string textfile, string replacement)
        {
            Char[] meowarray = soregular.ToCharArray();
            List<char> catlist = new List<char>();

            foreach (char z in meowarray)
            {
                if (z == '(')
                {
                    continue;
                }
                else if (z == ')')
                {
                    break;
                }
                if (z == '.')
                {
                    continue;
                }
                if (z.Equals(null))
                {
                    break;
                }
                else
                    catlist.Add(z);

            }
            StringBuilder builder = new StringBuilder();
            foreach (char p in catlist)
            {
                builder.Append(p);
            }
            string result = builder.ToString();

            //read and edit doc
            using (var sourceFile = File.OpenText(@textfile))
            {
                StreamReader sr = new StreamReader(@textfile);
                string line = sr.ReadToEnd();

                while ((sourceFile.ReadLine()) != null)
                {
                    line = line.Replace(result, replacement);
                    Console.WriteLine(line);
                }
            }
        }
    }
}
